import {Entity, model, property} from '@loopback/repository';

@model()
export class Detail extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  idpublication: number;

  @property({
    type: 'number',
    required: true,
  })
  idtag: number;


  constructor(data?: Partial<Detail>) {
    super(data);
  }
}

export interface DetailRelations {
  // describe navigational properties here
}

export type DetailWithRelations = Detail & DetailRelations;
