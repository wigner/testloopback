import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Publication} from '../models';
import {PublicationRepository} from '../repositories';

export class PublicationController {
  constructor(
    @repository(PublicationRepository)
    public publicationRepository : PublicationRepository,
  ) {}

  @post('/publications', {
    responses: {
      '200': {
        description: 'Publication model instance',
        content: {'application/json': {schema: getModelSchemaRef(Publication)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Publication, {
            title: 'NewPublication',
            exclude: ['idpublication'],
          }),
        },
      },
    })
    publication: Omit<Publication, 'idpublication'>,
  ): Promise<Publication> {
    return this.publicationRepository.create(publication);
  }

  @get('/publications/count', {
    responses: {
      '200': {
        description: 'Publication model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Publication) where?: Where<Publication>,
  ): Promise<Count> {
    return this.publicationRepository.count(where);
  }

  @get('/publications', {
    responses: {
      '200': {
        description: 'Array of Publication model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Publication, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Publication) filter?: Filter<Publication>,
  ): Promise<Publication[]> {
    return this.publicationRepository.find(filter);
  }

  @patch('/publications', {
    responses: {
      '200': {
        description: 'Publication PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Publication, {partial: true}),
        },
      },
    })
    publication: Publication,
    @param.where(Publication) where?: Where<Publication>,
  ): Promise<Count> {
    return this.publicationRepository.updateAll(publication, where);
  }

  @get('/publications/{id}', {
    responses: {
      '200': {
        description: 'Publication model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Publication, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Publication, {exclude: 'where'}) filter?: FilterExcludingWhere<Publication>
  ): Promise<Publication> {
    return this.publicationRepository.findById(id, filter);
  }

  @patch('/publications/{id}', {
    responses: {
      '204': {
        description: 'Publication PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Publication, {partial: true}),
        },
      },
    })
    publication: Publication,
  ): Promise<void> {
    await this.publicationRepository.updateById(id, publication);
  }

  @put('/publications/{id}', {
    responses: {
      '204': {
        description: 'Publication PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() publication: Publication,
  ): Promise<void> {
    await this.publicationRepository.replaceById(id, publication);
  }

  @del('/publications/{id}', {
    responses: {
      '204': {
        description: 'Publication DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.publicationRepository.deleteById(id);
  }
}
