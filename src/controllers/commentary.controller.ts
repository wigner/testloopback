import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Commentary} from '../models';
import {CommentaryRepository} from '../repositories';

export class CommentaryController {
  constructor(
    @repository(CommentaryRepository)
    public commentaryRepository : CommentaryRepository,
  ) {}

  @post('/commentaries', {
    responses: {
      '200': {
        description: 'Commentary model instance',
        content: {'application/json': {schema: getModelSchemaRef(Commentary)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Commentary, {
            title: 'NewCommentary',
            exclude: ['idcommentary'],
          }),
        },
      },
    })
    commentary: Omit<Commentary, 'idcommentary'>,
  ): Promise<Commentary> {
    return this.commentaryRepository.create(commentary);
  }

  @get('/commentaries/count', {
    responses: {
      '200': {
        description: 'Commentary model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Commentary) where?: Where<Commentary>,
  ): Promise<Count> {
    return this.commentaryRepository.count(where);
  }

  @get('/commentaries', {
    responses: {
      '200': {
        description: 'Array of Commentary model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Commentary, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Commentary) filter?: Filter<Commentary>,
  ): Promise<Commentary[]> {
    return this.commentaryRepository.find(filter);
  }

  @patch('/commentaries', {
    responses: {
      '200': {
        description: 'Commentary PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Commentary, {partial: true}),
        },
      },
    })
    commentary: Commentary,
    @param.where(Commentary) where?: Where<Commentary>,
  ): Promise<Count> {
    return this.commentaryRepository.updateAll(commentary, where);
  }

  @get('/commentaries/{id}', {
    responses: {
      '200': {
        description: 'Commentary model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Commentary, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Commentary, {exclude: 'where'}) filter?: FilterExcludingWhere<Commentary>
  ): Promise<Commentary> {
    return this.commentaryRepository.findById(id, filter);
  }

  @patch('/commentaries/{id}', {
    responses: {
      '204': {
        description: 'Commentary PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Commentary, {partial: true}),
        },
      },
    })
    commentary: Commentary,
  ): Promise<void> {
    await this.commentaryRepository.updateById(id, commentary);
  }

  @put('/commentaries/{id}', {
    responses: {
      '204': {
        description: 'Commentary PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() commentary: Commentary,
  ): Promise<void> {
    await this.commentaryRepository.replaceById(id, commentary);
  }

  @del('/commentaries/{id}', {
    responses: {
      '204': {
        description: 'Commentary DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.commentaryRepository.deleteById(id);
  }
}
