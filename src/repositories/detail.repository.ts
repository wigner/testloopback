import {DefaultCrudRepository} from '@loopback/repository';
import {Detail, DetailRelations} from '../models';
import {DbloopbackDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DetailRepository extends DefaultCrudRepository<
  Detail,
  typeof Detail.prototype.idpublication,
  DetailRelations
> {
  constructor(
    @inject('datasources.dbloopback') dataSource: DbloopbackDataSource,
  ) {
    super(Detail, dataSource);
  }
}
