import {DefaultCrudRepository} from '@loopback/repository';
import {Publication, PublicationRelations} from '../models';
import {DbloopbackDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class PublicationRepository extends DefaultCrudRepository<
  Publication,
  typeof Publication.prototype.idpublication,
  PublicationRelations
> {
  constructor(
    @inject('datasources.dbloopback') dataSource: DbloopbackDataSource,
  ) {
    super(Publication, dataSource);
  }
}
