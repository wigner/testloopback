import {DefaultCrudRepository} from '@loopback/repository';
import {User, UserRelations} from '../models';
import {DbloopbackDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.iduser,
  UserRelations
> {
  constructor(
    @inject('datasources.dbloopback') dataSource: DbloopbackDataSource,
  ) {
    super(User, dataSource);
  }
}
