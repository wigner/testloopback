import {DefaultCrudRepository} from '@loopback/repository';
import {Tag, TagRelations} from '../models';
import {DbloopbackDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TagRepository extends DefaultCrudRepository<
  Tag,
  typeof Tag.prototype.idtag,
  TagRelations
> {
  constructor(
    @inject('datasources.dbloopback') dataSource: DbloopbackDataSource,
  ) {
    super(Tag, dataSource);
  }
}
