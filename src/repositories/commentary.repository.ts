import {DefaultCrudRepository} from '@loopback/repository';
import {Commentary, CommentaryRelations} from '../models';
import {DbloopbackDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CommentaryRepository extends DefaultCrudRepository<
  Commentary,
  typeof Commentary.prototype.idcommentary,
  CommentaryRelations
> {
  constructor(
    @inject('datasources.dbloopback') dataSource: DbloopbackDataSource,
  ) {
    super(Commentary, dataSource);
  }
}
